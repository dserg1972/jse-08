package com.nlmk.dezhemesov.taskmanager.dao;

import com.nlmk.dezhemesov.taskmanager.entity.Project;
import com.nlmk.dezhemesov.taskmanager.entity.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Хранилище задач
 */
public class TaskDAO {

    /**
     * Список задач
     */
    private List<Task> tasks = new ArrayList<>();

    {
        create("TASK1");
        create("TASK2");
    }

    /**
     * Создание задачи и добавление в хранилище
     *
     * @param name имя задачи
     * @return созданная задача
     */
    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        return task;
    }

    /**
     * Очистка хранилища
     */
    public void clear() {
        tasks.clear();
    }

    /**
     * Список задач
     *
     * @return список задач
     */
    public List<Task> findAll() {
        return tasks;
    }

    /**
     * Поиск задачи в хранилище по индексу
     *
     * @param index индекс задачи в хранилище
     * @return найденная задача либо null, если задача не найдена либо неправильный индекс
     */
    public Task findByIndex(int index) {
        if (index < 0 || index > tasks.size())
            return null;
        return tasks.get(index);
    }

    /**
     * Поиск задачи в хранилище по имени
     *
     * @param name имя задачи
     * @return найденная задача либо null, если задача не найдена
     */
    public Task findByName(String name) {
        if (name == null)
            return null;
        for (Task task : tasks)
            if (task.getName().equals(name))
                return task;
        return null;
    }

    /**
     * Поиск задачи в хранилище по идентификатору
     *
     * @param id идентификатор задачи
     * @return найденная задача либо null, если задача не найдена
     */
    public Task findById(Long id) {
        if (id == null)
            return null;
        for (Task task : tasks)
            if (task.getId().equals(id))
                return task;
        return null;
    }

    public static void main(String[] args) {
        TaskDAO dao = new TaskDAO();
        Task task = dao.findByName("TASK2");
        System.out.println(task);
    }

}
