package com.nlmk.dezhemesov.taskmanager.dao;

import com.nlmk.dezhemesov.taskmanager.entity.Project;

import java.util.ArrayList;
import java.util.List;

/**
 * Хранилище проектов
 */
public class ProjectDAO {

    /**
     * Список проектов
     */
    private List<Project> projects = new ArrayList<>();

    {
        create("PROJECT1");
        create("PROJECT2");
    }

    /**
     * Создание проекта и добавление в хранилище
     *
     * @param name имя проекта
     * @return созданный проект
     */
    public Project create(final String name) {
        final Project project = new Project(name);
        projects.add(project);
        return project;
    }

    /**
     * Очистка хранилища
     */
    public void clear() {
        projects.clear();
    }

    /**
     * Возврат ссылки на хранилище
     *
     * @return ссылка на хранилище
     */
    public List<Project> findAll() {
        return projects;
    }

    /**
     * Поиск проекта в хранилище по индексу
     *
     * @param index индекс проекта в хранилище
     * @return найденный проект либо null, если проект не найден либо неправильный индекс
     */
    public Project findByIndex(int index) {
        if (index < 0 || index > projects.size())
            return null;
        return projects.get(index);
    }

    /**
     * Поиск проекта в хранилище по имени
     *
     * @param name имя проекта
     * @return найденный проект либо null, если проект не найден
     */
    public Project findByName(String name) {
        if (name == null)
            return null;
        for (Project project : projects)
            if (project.getName().equals(name))
                return project;
        return null;
    }

    /**
     * Поиск проекта в хранилище по идентификатору
     *
     * @param id идентификатор проекта
     * @return найденный проект либо null, если проект не найден
     */
    public Project findById(Long id) {
        if (id == null)
            return null;
        for (Project project : projects)
            if (project.getId().equals(id))
                return project;
        return null;
    }

    public static void main(String[] args) {
        ProjectDAO dao = new ProjectDAO();
        Project project = dao.findByName("PROJECT2");
        System.out.println(project);
    }

}
