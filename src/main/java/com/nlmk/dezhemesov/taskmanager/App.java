package com.nlmk.dezhemesov.taskmanager;

import com.nlmk.dezhemesov.taskmanager.dao.ProjectDAO;
import com.nlmk.dezhemesov.taskmanager.dao.TaskDAO;
import com.nlmk.dezhemesov.taskmanager.entity.Project;
import com.nlmk.dezhemesov.taskmanager.entity.Task;

import java.util.Scanner;

import static com.nlmk.dezhemesov.taskmanager.constants.Console.*;

/**
 * Консольный модуль. Точка входа в приложение
 */

public class App {

    /**
     * Хранилище проектов
     */
    private static final ProjectDAO projects = new ProjectDAO();

    /**
     * Хранилище задач
     */
    private static final TaskDAO tasks = new TaskDAO();

    /**
     * Обработчик системного ввода
     */
    private static final Scanner scanner = new Scanner(System.in);

    /**
     * Точка входа в приложение
     *
     * @param args - аргументы командной строки
     */
    public static void main(final String[] args) {
        run(args);
        displayWelcome();
        process();
    }

    /**
     * Чтение команды из системного ввода
     *
     * @return команда
     */
    private static String readString() {
        String command;
        command = scanner.nextLine();
        return command;
    }

    /**
     * Чтение целочисленного значения из системного ввода
     *
     * @return введённая строка, интерпретированная как целочисленное значение
     */
    private static Integer readInteger() {

        Integer value = null;
        try {
            value = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException ex) {
            System.out.println("Can't parse integer value");
        }
        return value;
    }

    /**
     * Чтение Long значения из системного ввода
     *
     * @return введённая строка, интерпретированная как Long значение
     */
    private static Long readLong() {

        Long value = null;
        try {
            value = Long.parseLong(scanner.nextLine());
        } catch (NumberFormatException ex) {
            System.out.println("Can't parse Long value");
        }
        return value;
    }

    /**
     * Основной цикл обработки интерактивных команд
     */
    private static void process() {
        String command = "";
        while (!EXIT.equals(command)) {
            command = readString();
            run(command);
            System.out.println();
        }
    }

    /**
     * Обработчик ключей запуска
     *
     * @param args аргументы командной строки
     */
    private static void run(String[] args) {
        if (args == null) return;
        if (args.length < 1) return;
        String command = args[0];
        int result = run(command);
        System.exit(result);
    }

    /**
     * Обработчик команды
     *
     * @param command команда на исполнение
     * @return результат выполнения команды
     */
    private static int run(final String command) {
        if (command == null || command.isEmpty()) return -1;
        switch (command) {
            case VERSION:
                return displayVersion();
            case ABOUT:
                return displayAbout();
            case HELP:
                return displayHelp();
            case EXIT:
                return exit();
            case PROJECT_CREATE:
                return createProject();
            case PROJECT_LIST:
                return listProject();
            case PROJECT_CLEAR:
                return clearProject();
            case TASK_CREATE:
                return createTask();
            case TASK_LIST:
                return listTask();
            case TASK_CLEAR:
                return clearTask();

            case PROJECT_VIEW_BY_INDEX:
                return viewProjectByIndex();
            case PROJECT_VIEW_BY_ID:
                return viewProjectById();
            case PROJECT_VIEW_BY_NAME:
                return viewProjectByName();
            case PROJECT_EDIT_BY_INDEX:
                return editProjectByIndex();
            case PROJECT_EDIT_BY_ID:
                return editProjectById();
            case PROJECT_EDIT_BY_NAME:
                return editProjectByName();
            case PROJECT_REMOVE_BY_INDEX:
                return removeProjectByIndex();
            case PROJECT_REMOVE_BY_ID:
                return removeProjectById();
            case PROJECT_REMOVE_BY_NAME:
                return removeProjectByName();

            case TASK_VIEW_BY_INDEX:
                return viewTaskByIndex();
            case TASK_VIEW_BY_ID:
                return viewTaskById();
            case TASK_VIEW_BY_NAME:
                return viewTaskByName();
            case TASK_EDIT_BY_INDEX:
                return editTaskByIndex();
            case TASK_EDIT_BY_ID:
                return editTaskById();
            case TASK_EDIT_BY_NAME:
                return editTaskByName();
            case TASK_REMOVE_BY_INDEX:
                return removeTaskByIndex();
            case TASK_REMOVE_BY_ID:
                return removeTaskById();
            case TASK_REMOVE_BY_NAME:
                return removeTaskByName();


            default:
                return displayError();
        }
    }

    /**
     * Добавление проекта в хранилище
     *
     * @return код возврата
     */
    private static int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter project name: ");
        final String name = readString();
        projects.create(name);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Вывод списка проектов в хранилище
     *
     * @return код возврата
     */
    private static int listProject() {
        System.out.println("[LIST PROJECT]");
        int i = 1;
        for (Project project : projects.findAll())
            System.out.println("[" + (i++) + "]: " + project);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Удаление всех проектов из хранилища
     *
     * @return код возврата
     */
    private static int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projects.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Добавление задачи в хранилище
     *
     * @return код возврата
     */
    private static int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.print("Enter task name: ");
        final String name = readString();
        tasks.create(name);

        System.out.println("[OK]");
        return 0;
    }

    /**
     * Вывод списка задач в хранилище
     *
     * @return код возврата
     */
    private static int listTask() {
        System.out.println("[LIST TASK]");
        int i = 1;
        for (Task task : tasks.findAll())
            System.out.println("[" + (i++) + "]: " + task);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Удаление всех задач из хранилища
     *
     * @return код возврата
     */
    private static int clearTask() {
        System.out.println("[CLEAR TASK]");
        tasks.clear();
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Вывод строки-приветствия
     */
    private static void displayWelcome() {
        System.out.println("*** WELCOME TO THE TASK MANAGER ***");
    }

    /**
     * Сведения о программе
     */
    private static int displayAbout() {
        System.out.println("Author: Serge Dezhemesov");
        System.out.println("        dserg1972@gmail.com");
        return 0;
    }

    /**
     * Сведенеия о версии
     */
    private static int displayVersion() {
        System.out.println("Version: 1.0.0");
        return 0;
    }

    /**
     * Перечень команд
     */
    private static int displayHelp() {
        System.out.println("Usage: java -jar taskmanager.jar [command]");
        System.out.println("Commands:");
        System.out.println("  about   - display developer info");
        System.out.println("  help    - display usage");
        System.out.println("  version - display version info");
        System.out.println("  exit    - terminate program");
        System.out.println();
        System.out.println("  project-create - create new project");
        System.out.println("  project-list   - display list of projects");
        System.out.println("  project-clear  - remove all projects");
        System.out.println();
        System.out.println("  task-create - create new task");
        System.out.println("  task-list   - display list of tasks");
        System.out.println("  task-clear  - remove all task");
        System.out.println();
        System.out.println("  project-view-by-index - view project properties by index");
        System.out.println("  project-view-by-name  - view project properties by name");
        System.out.println("  project-view-by-id    - view project properties by identifier");
        System.out.println();
        System.out.println("  project-edit-by-index - change project properties by index");
        System.out.println("  project-edit-by-name  - change project properties by name");
        System.out.println("  project-edit-by-id    - change project properties by identifier");
        System.out.println();
        System.out.println("  project-remove-by-index - remove project by index");
        System.out.println("  project-remove-by-name  - remove project by name");
        System.out.println("  project-remove-by-id    - remove project by identifier");
        System.out.println();
        System.out.println("  task-view-by-index - view task properties by index");
        System.out.println("  task-view-by-name  - view task properties by name");
        System.out.println("  task-view-by-id    - view task properties by identifier");
        System.out.println();
        System.out.println("  task-edit-by-index - change task properties by index");
        System.out.println("  task-edit-by-name  - change task properties by name");
        System.out.println("  task-edit-by-id    - change task properties by identifier");
        System.out.println();
        System.out.println("  task-remove-by-index - remove task by index");
        System.out.println("  task-remove-by-name  - remove task by name");
        System.out.println("  task-remove-by-id    - remove task by identifier");
        return 0;
    }

    /**
     * Сообщение об ошибочной команде
     */
    private static int displayError() {
        System.out.println("Unknown command");
        return -1;
    }

    /**
     * Обработка команды выхода из программы
     *
     * @return результат вызова
     */
    private static int exit() {
        System.out.println("Terminating program ...");
        System.exit(0);
        return 0;
    }

    /**
     * Просмотр атрибутов проекта
     *
     * @param project проект
     * @return проект
     */
    private static Project viewProject(final Project project) {
        if (project == null)
            return null;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("[OK]");
        return project;
    }

    /**
     * Редактирование атрибутов проекта
     *
     * @param project проект
     * @return проект
     */
    private static Project editProject(final Project project) {
        if (project == null)
            return null;
        System.out.println("[EDIT PROJECT]");
        System.out.print("Enter name: ");
        String name = readString();
        project.setName(name);
        System.out.print("Enter description: ");
        String description = readString();
        project.setDescription(description);
        System.out.println("[OK]");
        return project;
    }

    /**
     * Удаление проекта из хранилища
     *
     * @param project проект
     * @return проект
     */
    private static Project removeProject(final Project project) {
        if (project == null)
            return null;
        System.out.println("[REMOVE PROJECT]");
        if (projects.findAll().remove(project))
            System.out.println("[OK]");
        else
            System.out.println("SOMETHING WRONG");
        return project;
    }

    /**
     * Просмотр атрибутов задачи
     *
     * @param task задача
     * @return задача            
     */
    private static Task viewTask(final Task task) {
        if (task == null)
            return null;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("[OK]");
        return task;
    }

    /**
     * Редактирование атрибутов задачи
     *
     * @param task задача
     * @return задача
     */
    private static Task editTask(final Task task) {
        if (task == null)
            return null;
        System.out.println("[EDIT TASK]");
        System.out.print("Enter name: ");
        String name = readString();
        task.setName(name);
        System.out.print("Enter description: ");
        String description = readString();
        task.setDescription(description);
        System.out.println("[OK]");
        return task;
    }

    /**
     * Удаление задачи из хранилища
     *
     * @param task задача
     * @return задача
     */
    private static Task removeTask(final Task task) {
        if (task == null)
            return null;
        System.out.println("[REMOVE TASK]");
        if (tasks.findAll().remove(task))
            System.out.println("[OK]");
        else
            System.out.println("SOMETHING WRONG");
        return task;
    }

    /**
     * Просмотр проекта по индексу
     *
     * @return код возврата
     */
    private static int viewProjectByIndex() {
        System.out.print("Enter index: ");
        int index = readInteger();
        Project project = projects.findByIndex(index - 1);
        viewProject(project);
        return 0;
    }

    /**
     * Просмотр проекта по идентификатору
     *
     * @return код возврата
     */
    private static int viewProjectById() {
        System.out.print("Enter id: ");
        Long id = readLong();
        Project project = projects.findById(id);
        viewProject(project);
        return 0;
    }

    /**
     * Просмотр проекта по имени
     *
     * @return код возврата
     */
    private static int viewProjectByName() {
        System.out.print("Enter name: ");
        String name = readString();
        Project project = projects.findByName(name);
        viewProject(project);
        return 0;
    }

    /**
     * Редкатирование проекта по индексу
     *
     * @return код возврата
     */
    private static int editProjectByIndex() {
        System.out.print("Enter index: ");
        int index = readInteger();
        Project project = projects.findByIndex(index - 1);
        editProject(project);
        return 0;
    }

    /**
     * Редактирование проекта по идентификатору
     *
     * @return код возврата
     */
    private static int editProjectById() {
        System.out.print("Enter id: ");
        Long id = readLong();
        Project project = projects.findById(id);
        editProject(project);
        return 0;
    }

    /**
     * Редактирование проекта по имени
     *
     * @return код возврата
     */
    private static int editProjectByName() {
        System.out.print("Enter name: ");
        String name = readString();
        Project project = projects.findByName(name);
        editProject(project);
        return 0;
    }

    /**
     * Удаление проекта по индексу
     *
     * @return код возврата
     */
    private static int removeProjectByIndex() {
        System.out.print("Enter index: ");
        int index = readInteger();
        Project project = projects.findByIndex(index - 1);
        removeProject(project);
        return 0;
    }

    /**
     * Удаление проекта по идентификатору
     *
     * @return код возврата
     */
    private static int removeProjectById() {
        System.out.print("Enter id: ");
        Long id = readLong();
        Project project = projects.findById(id);
        removeProject(project);
        return 0;
    }

    /**
     * Удаление проекта по имени
     *
     * @return код возврата
     */
    private static int removeProjectByName() {
        System.out.print("Enter name: ");
        String name = readString();
        Project project = projects.findByName(name);
        removeProject(project);
        return 0;
    }

    /**
     * Просмотр задачи по индексу
     *
     * @return код возврата
     */
    private static int viewTaskByIndex() {
        System.out.print("Enter index: ");
        int index = readInteger();
        Task task = tasks.findByIndex(index - 1);
        viewTask(task);
        return 0;
    }

    /**
     * Просмотр задачи по идентификатору
     *
     * @return код возврата
     */
    private static int viewTaskById() {
        System.out.print("Enter id: ");
        Long id = readLong();
        Task task = tasks.findById(id);
        viewTask(task);
        return 0;
    }

    /**
     * Просмотр задачи по имени
     *
     * @return код возврата
     */
    private static int viewTaskByName() {
        System.out.print("Enter name: ");
        String name = readString();
        Task task = tasks.findByName(name);
        viewTask(task);
        return 0;
    }

    /**
     * Редкатирование задачи по индексу
     *
     * @return код возврата
     */
    private static int editTaskByIndex() {
        System.out.print("Enter index: ");
        int index = readInteger();
        Task task = tasks.findByIndex(index - 1);
        editTask(task);
        return 0;
    }

    /**
     * Редактирование задачи по идентификатору
     *
     * @return код возврата
     */
    private static int editTaskById() {
        System.out.print("Enter id: ");
        Long id = readLong();
        Task task = tasks.findById(id);
        editTask(task);
        return 0;
    }

    /**
     * Редактирование задачи по имени
     *
     * @return код возврата
     */
    private static int editTaskByName() {
        System.out.print("Enter name: ");
        String name = readString();
        Task task = tasks.findByName(name);
        editTask(task);
        return 0;
    }

    /**
     * Удаление задачи по индексу
     *
     * @return код возврата
     */
    private static int removeTaskByIndex() {
        System.out.print("Enter index: ");
        int index = readInteger();
        Task task = tasks.findByIndex(index - 1);
        removeTask(task);
        return 0;
    }

    /**
     * Удаление задачи по идентификатору
     *
     * @return код возврата
     */
    private static int removeTaskById() {
        System.out.print("Enter id: ");
        Long id = readLong();
        Task task = tasks.findById(id);
        removeTask(task);
        return 0;
    }

    /**
     * Удаление задачи по имени
     *
     * @return код возврата
     */
    private static int removeTaskByName() {
        System.out.print("Enter name: ");
        String name = readString();
        Task task = tasks.findByName(name);
        removeTask(task);
        return 0;
    }

}
