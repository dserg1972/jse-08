package com.nlmk.dezhemesov.taskmanager.constants;

/**
 * Константные значения для использования в консольном модуле
 */
public class Console {
    /**
     * Сведения об авторе
     */
    public static final String ABOUT = "about";
    /**
     * Помощь по ключам запуска
     */
    public static final String HELP = "help";
    /**
     * Сведения о версии
     */
    public static final String VERSION = "version";
    /**
     * Выход из программы
     */
    public static final String EXIT = "exit";

    /**
     * Создание проекта
     */
    public static final String PROJECT_CREATE = "project-create";
    /**
     * Список проектов
     */
    public static final String PROJECT_LIST = "project-list";
    /**
     * Удаление всех проектов
     */
    public static final String PROJECT_CLEAR = "project-erase";

    /**
     * Создание задачи
     */
    public static final String TASK_CREATE = "task-create";
    /**
     * Список задач
     */
    public static final String TASK_LIST = "task-list";
    /**
     * Удаление всех задач
     */
    public static final String TASK_CLEAR = "task-clear";


    /**
     * Просмотр проекта по индексу
     */
    public static final String PROJECT_VIEW_BY_INDEX = "project-view-by-index";
    /**
     * Просмотр проекта по имени
     */
    public static final String PROJECT_VIEW_BY_NAME = "project-view-by-name";
    /**
     * Просмотр проекта по идентификатору
     */
    public static final String PROJECT_VIEW_BY_ID = "project-view-by-id";

    /**
     * Удаление проекта по индексу
     */
    public static final String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";
    /**
     * Удаление проекта по имени
     */
    public static final String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";
    /**
     * Удаление проекта по идентификатору
     */
    public static final String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    /**
     * Редактирование проекта по индексу
     */
    public static final String PROJECT_EDIT_BY_INDEX = "project-edit-by-index";
    /**
     * Редактирование проекта по имени
     */
    public static final String PROJECT_EDIT_BY_NAME = "project-edit-by-name";
    /**
     * Редактирование проекта по идентификатору
     */
    public static final String PROJECT_EDIT_BY_ID = "project-edit-by-id";


    /**
     * Просмотр задачи по индексу
     */
    public static final String TASK_VIEW_BY_INDEX = "task-view-by-index";
    /**
     * Просмотр задачи по имени
     */
    public static final String TASK_VIEW_BY_NAME = "task-view-by-name";
    /**
     * Просмотр задачи по идентификатору
     */
    public static final String TASK_VIEW_BY_ID = "task-view-by-id";

    /**
     * Удаление задачи по индексу
     */
    public static final String TASK_REMOVE_BY_INDEX = "task-remove-by-index";
    /**
     * Удаление задачи по имени
     */
    public static final String TASK_REMOVE_BY_NAME = "task-remove-by-name";
    /**
     * Удаление задачи по идентификатору
     */
    public static final String TASK_REMOVE_BY_ID = "task-remove-by-id";

    /**
     * Редактирование задачи по индексу
     */
    public static final String TASK_EDIT_BY_INDEX = "task-edit-by-index";
    /**
     * Редактирование задачи по имени
     */
    public static final String TASK_EDIT_BY_NAME = "task-edit-by-name";
    /**
     * Редактирование задачи по идентификатору
     */
    public static final String TASK_EDIT_BY_ID = "task-edit-by-id";
}
